#instr = "prawidłowe wywołanie:\nreader.py <src> <dst> <change1> <change2>" \
        # "\nchange1 ... changeN to ciągi znaków w postaci 'Y,X,wartosc'" \
        # "\nY to wiersz do zmodyfikowania (liczony od 0), X to kolumna " \
        # "(liczona od 0). <src> i <dst> pliki .csv, .json, .pickle "

import sys
import os
import pickle
import json


class ReadFile:
    def __init__(self):
        self.file_in, self.file_out = sys.argv[1], sys.argv[2]
        self.changes = sys.argv[3:]
        self.contents = []

    def make_changes(self):
        print(self.contents)
        for indx in range(len(self.changes)):
            row = int(self.changes[indx].split(',')[0])
            col = int(self.changes[indx].split(',')[1])
            val =     self.changes[indx].split(',')[2]
            self.contents[row][col] = val
        print(self.contents)


class ReadFileCSV(ReadFile):

    def read_file(self):
        with open(self.file_in) as f:
            for line in f:
                self.contents.append(line.replace('\n', '').split(','))

    def write_file(self):
        with open(self.file_out, "w+") as f:
            for indx in range(len(self.contents)):
                f.write(','.join(self.contents[indx])+'\n')


class ReadFileJSON(ReadFile):

    def read_file(self):
        with open(self.file_in) as f:
            self.contents = json.load(f)

    def write_file(self):
        with open(self.file_out, "w+") as f:
            f.write(json.dumps(self.contents))


class ReadFilePICKLE(ReadFile):

    def read_file(self):
        with open(self.file_in, 'rb') as f:
            self.contents = pickle.load(f)

    def write_file(self):
        with open(self.file_out, "wb") as f:
            pickle.dump(self.contents, f)

def file_check (file_name):


#TODO test file opening

# INTERPRET INPUT FILE'S EXTENSION
file_in_ext = sys.argv[1].split('.')[-1]
if file_in_ext == 'csv': obj = ReadFileCSV()
elif file_in_ext == 'json': obj = ReadFileJSON()
elif file_in_ext == 'pickle': obj = ReadFilePICKLE()

obj.read_file()
obj.make_changes()

# INTERPRET OUTPUT FILE'S EXTENSION
file_out_ext = sys.argv[2].split('.')[-1]
if file_in_ext != file_out_ext:
    if file_out_ext == 'csv': obj_out = ReadFileCSV()
    elif file_out_ext == 'json': obj_out = ReadFileJSON()
    elif file_out_ext == 'pickle': obj_out = ReadFilePICKLE()
    obj_out.contents = obj.contents
    obj_out.write_file()
else:
    obj.write_file()
