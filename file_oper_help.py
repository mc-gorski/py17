# przykładowe wywołanie:
#python3 reader.py path1/plik1 path2 0,00,AAA 01,1,"BB B !" 2,2,CC_C

import csv
import sys
import os

# INPUT FILE: THERE IS NO SUCH FILE, BUT PATH IS CORRECTLY GIVEN
if not os.path.isfile(sys.argv[1]) \
        and os.path.exists(os.path.dirname(sys.argv[1])):
    print(f"- "*40, "\n", " "*15, "!!! Tylko ścieżka jest OK (pliku brak) "
          f"!!!\n", " "*23, "Są tam następujące pliki:\n", "- "*40)
    os.chdir(os.path.dirname(sys.argv[1]))
    os.system('find . -maxdepth 1 -not -type d -ls')

# INPUT FILE: BOTH PATH AND FILE ARE WRONG
elif not os.path.exists(sys.argv[1]):
    print(f"- "*40, "\n", "  !!! błędny obiekt wejściowy: podano błędną ścieżkę"
          f" lub nazwę pliku !!!\n", "- "*40, "\nkatalog roboczy programu:")
    os.system('pwd')
    print(f"- "*40, "\n", "są w nim następujące pliki:")
    os.system('find . -maxdepth 1 -not -type d -ls')

# TOO FEW INPUT DATA
elif len(sys.argv) < 4:
    print(f"!!! zbyt mało argumentów !!! \nprzykładowy sposób wywołania:\n"
          f"python reader.py path_in/plik_in path_out 0,0,AAA 1,1,BBB 2,2,CCC")

# INPUT FILE IS CORRECTLY POINTED
elif os.path.isfile(sys.argv[1]):
    file_in = sys.argv[1]

    # READ INPUT FILE
    all_lines = []
    with open(file_in, newline="") as csv_file:
        content = csv.reader(csv_file)
        for line in content:
            all_lines.append(line)

    # READ CHANGES COMMANDS
    all_changes = []
    for index in range(3, len(sys.argv)):
        all_changes.append(sys.argv[index].split(","))

    # MAKE CHANGES
    for index in range(0, len(all_changes)):
        x = int(all_changes[index][0])
        y = int(all_changes[index][1])
        all_lines[x][y] = all_changes[index][2]

    # AND SHOW IT
    for row in all_lines:
        print(" ".join(row))

    ### DECIDE OUTPUT FILE ###

    # OUTPUT FILE IS DIRECTLY POINTED AND IT EXISTS
    if os.path.isfile(sys.argv[2]):
        file_out = sys.argv[2]

    # ONLY OUTPUT DIRECTORY IS GIVEN
    # (this is exactly what follows from the content of the task:
    # "dst to docelowa ŚCIEŻKA, w której zapiszemy zmieniony plik csv."
    # so: we take (from the user's command) only name of the path,
    # name of the file remains not changed, just content will be changed
    elif os.path.isdir(sys.argv[2]) and os.path.exists(sys.argv[2]):
        file_out = (sys.argv[2]) + os.sep + os.path.basename(sys.argv[1])

    # THE PATH EXISTS BUT FILE DOESN'T (THEN FILE WILL BE CREATED)
    elif os.path.exists(os.path.dirname(sys.argv[2])) \
            and not os.path.exists(sys.argv[2]):
        file_out = sys.argv[2]

    # THERE IS WRONG/NO PATH - ONLY FILE'S NAME IS TAKEN, DIR SET DEFAULT
    else:
        file_out = os.path.basename(sys.argv[2])
        print(f"błędna ścieżka do pliku wynikowego, plik '{file_out}' "
              f"zapisuję w katalogu:")
        os.system('pwd')
        print("wrong path")

    # WRITE OUTPUT DATA TO OUTPUT FILE
    with open(file_out, "w+", newline="") as csv_file:
        writer = csv.writer(csv_file)
        writer.writerows(all_lines)

# NO ONE EXPECTS THE SPANISH INQUISITION
else:
    print("!!! błędne zakończenie programu !!!")